"""
    symboltrades2df(symbol::String)

Download trades for one symbol.
Return `nothing` if no trade was found.
"""
function symboltrades2df(symbol::String)
    status, data = getMyTrades(symbol)
    if status == 429
        @warn "Backing off for 429s."
        sleep(429)
    end
    # if no trade on symbol, then return nothing
    isempty(data) && return nothing
    df = DataFrame(data)
    df.price = parse.(Float64, df.price)
    df.qty = parse.(Float64, df.qty)
    df.quoteQty = parse.(Float64, df.quoteQty)
    df.commission = parse.(Float64, df.commission)
    return df
end

"""
    queryprices!(pricedf)

Query the prices with `getAvgPrice()`.

# Example

```julia-repl
julia> psymbols = deepcopy(PRICE_SYMBOLS);

julia> queryprices!(psymbols)
```
"""
function queryprices!(pricedf)
    getsymbolprice(sym) = sym == "BUSDBUSD" ? 1.0 : parse(Float64, getAvgPrice(sym).data.price)
    pricedf[!, :price] = getsymbolprice.(pricedf[!, :symbol])
    return pricedf
end

"""
    downloadprices()

Download all. prices. from. Binance.
Note the "BUSDBUSD" symbol, that is appended.

# Example

```julia-repl
julia> downloadprices()
11×3 DataFrame
Row │ asset   symbol    price      
 │ String  String    Float64    
─────┼──────────────────────────────
1 │ BNB     BNBBUSD     426.2    
2 │ BTC     BTCBUSD   44970.8    
3 │ ETH     ETHBUSD    3174.27   
4 │ BETH    ETHBUSD    3174.27   
5 │ SOL     SOLBUSD     110.48   
6 │ DOGE    DOGEBUSD      0.1551 
7 │ LUNA    LUNABUSD     55.77   
8 │ FXS     FXSBUSD      21.185  
9 │ PYR     PYRBUSD      14.22   
10 │ BUSD    BUSDBUSD      1.0    
11 │ USDT    BUSDBUSD      1.0
```
"""
function downloadprices()
    ap = getAllPrices()
    df = DataFrame(ap.data)
    push!(df, ["BUSDBUSD", "1.0"])
    push!(df, ["USDTUSDT", "1.0"])
    df = leftjoin(PRICE_SYMBOLS, df, on=:symbol)
    df[!, :price] = parse.(Float64, df[!, :price])
    return df
end

function prices2csv(fname=CSV_PRICES)
    df = downloadprices()
    CSV.write(fname, df)
end

## file import/export

"""
    alltrades2df(symbollist)

Query symbols from Binance, then export them to a `DataFrame`.

# Usage

```julia-repl
julia> alltrades2df(BinanceDashboard.SYMBOLS);
```
"""
function alltrades2df(symbollist)
    # download symbols concurrently
    spawns = [@spawn symboltrades2df(symbol) for symbol in symbollist]
    df = DataFrame()
    for s in spawns
        result = fetch(s)
        result === nothing && continue
        append!(df, result)
    end
    # append auto invest transactions
    # that are added by hand
    if isfile(CSV_SAVINGS_ACCOUNT)
        auto_invest = DataFrame(CSV.File(CSV_SAVINGS_ACCOUNT, stringtype=String))
        return vcat(df, auto_invest)
    else
        return df
    end
end

"""
    alltrades2csv(symbollist, fname=CSV_TRADES)

Query symbols from Binance, then export them in a `DataFrame` to csv file.
Export filename is defaulting to `CSV_TRADES=trades.csv`.

# Example

```julia-repl
julia> alltrades2csv(BinanceDashboard.SYMBOLS, fname=CSV_TRADES)
"trades.csv"
```
"""
function alltrades2csv(symbollist, fname=CSV_TRADES)
    df = alltrades2df(symbollist)
    CSV.write(fname, df)
end

"""
    spotaccount2df()

Query spot account balance from Binance and export it to a `DataFrame`.
"""
function spotaccount2df()
    acc = getSpotBalance()
    df = DataFrame(asset=String[], quantity=Float64[])
    for balance in acc
        bfree = parse(Float64, balance.free)
        blocked = parse(Float64, balance.locked)
        push!(df, [balance.asset, bfree + blocked])
    end
    return df
end

"""
    spotaccount2csv(fname = CSV_SPOT_ACCOUNT)

Query account balance from Binance, then keep only spot wallet balance
and export it to csv file.
"""
function spotaccount2csv(fname=CSV_SPOT_ACCOUNT)
    df = spotaccount2df()
    CSV.write(fname, df)
end

"""
    simpleearnaccount2df()

Get Simple Earn account (both flexible and locked) and export it to a `DataFrame`.
"""
function simpleearnaccount2df()
    account = DataFrame(asset=String[], quantity=Float64[], simpleearntype=String[])
    flexsimple = getFlexibleSimpleEarnAccount()
    for r in flexsimple.rows
        amount = parse(Float64, r.totalAmount)
        push!(account, [r.asset, amount, "flexible"])
    end
    lockedsimple = getLockedSimpleEarnAccount()
    for r in lockedsimple.rows
        amount = parse(Float64, r.amount)
        push!(account, [r.asset, amount, "locked"])
    end
    return account
end

"""
    simpleearnaccount2csv(fname = CSV_SIMPLEEARN_ACCOUNT)

Get Savings account and save it to csv.
"""
function simpleearnaccount2csv(fname=CSV_SIMPLEEARN_ACCOUNT)
    dfla = simpleearnaccount2df()
    CSV.write(fname, dfla)
end

function futuresaccount2df()
    fab = getFuturesBalance()
    df = DataFrame(asset=String[], quantity=Float64[])
    for asset in fab
        balance = parse(Float64, asset["balance"])
        isapprox(0, balance) && continue
        push!(df, [asset["asset"], balance])
    end
    return df
end

function futuresaccount2csv(fname=CSV_FUTURES_ACCOUNT)
    df = futuresaccount2df()
    CSV.write(fname, df)
end

"""
    loadcsv2df(fname)

Load a csv file to `DataFrame`.
"""
loadcsv2df(fname) = DataFrame(CSV.File(fname, stringtype=String))

"""
downloadbinanceaccount2dfs()

Dowload all data available from Binance.
"""
function downloadbinanceaccount2dfs()
    spot = spotaccount2df()
    trades = alltrades2df(SYMBOLS)
    simpleearn = simpleearnaccount2df()
    return (spot=spot, trades=trades, simpleearn=simpleearn)
end

"""
    downloadbinanceaccount()

Dowload all data available from Binance.
"""
function downloadbinanceaccount()
    savingsaccount2csv()
    spotaccount2csv()
    alltrades2csv(SYMBOLS)
    simpleearnaccount2csv()
end

## helper functions

"""
    symbol2tokens(symbol)

Strip a symbol to (known) tokens.

# Example

```julia-repl
julia> df[1,:]
DataFrameRow
Row │ symbol    id        orderId    orderListId  price    qty      quoteQty  commission  commissionAsset  time           isBuyer  isMaker  isBestMatch 
 │ String15  Int64     Int64      Int64        Float64  Float64  Float64   Float64     String7          Int64          Bool     Bool     Bool        
─────┼───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
1 │ DOGEBUSD  1  1           -1     0.1     1.0      1.0         0.0  DOGE             1     true     true         true

julia> symbol2tokens(df[1,:])
(first = "DOGE", second = "BUSD", buysign = 1)
```
"""
function symbol2tokens(dfr)
    symbol = dfr[:symbol]
    for t in TOKENS
        i = findfirst(t, symbol)
        i === nothing && continue
        token1 = first(i) == 1 ? t : string(split(symbol, t)[1])
        token2 = first(i) == 1 ? string(split(symbol, t)[2]) : t
        sign_buy = dfr[:isBuyer] ? 1 : -1
        return (first=token1, second=token2, buysign=sign_buy)
    end
    return nothing
end

"""
    coin2busd(coin, quantity)

Convert any coin, token, whatever (price) to BUSD.
"""
function coin2busd(coin, quantity)
    coin == "EUR" && return EUR2USD * quantity
    coin == "USDT" && return quantity
    coin == "BUSD" && return quantity
    coin == "BNB" && return coin2busd("EUR", quantity * 421)
end

"""
    transactionprice(dfr)

Price and quantity for one transaction (one row).
Price is always positive, quantity is negative when selling.
"""
function transactionprice(dfr)
    tokens = symbol2tokens(dfr)
    tokens === nothing && return nothing
    qty = tokens.buysign * dfr[:qty]
    price = coin2busd(tokens.second, dfr[:price])
    return Dict("coin" => tokens.first, "quantity" => qty, "price" => price)
end

## balance functions

"""
    rowbalance(dfr)

From one `dfr::DataFrameRow`, calculate the +/- for each pair.
"""
function rowbalance(dfr)
    tokens = symbol2tokens(dfr)
    tokens === nothing && return nothing
    d1 = Dict(tokens.first => tokens.buysign * dfr[:qty])
    d2 = Dict(tokens.second => -1 * tokens.buysign * dfr[:quoteQty])
    # comission
    d3 = Dict(dfr[:commissionAsset] => -1 * dfr[:commission])
    return mergewith(+, d1, d2, d3)
end

"""
    accountbalance(df::DataFrame)

Calculate the balance based on trade history.
"""
accountbalance(df) = mergewith(+, rowbalance.(eachrow(df))...)

"""
    investmentbalance(df)

Account balance: total sum of sells and buys on a selected list of tokens (`IMPORTANT_TOKENS`).
"""
function investmentbalance(df)
    # query prices for all the rows (and calculate the sell/buy sign as well)
    rows = [transactionprice(row) for row in eachrow(df)]
    df = DataFrame(asset=String[], balance=Float64[])
    # only interested in "important tokens"
    for token in IMPORTANT_TOKENS
        # all transaction for one important token
        transactions = filter(x -> x["coin"] == token, rows)
        # sum every transaction: buy: +, sell: -
        isempty(transactions) && continue
        sum_price = mapreduce(x -> x["price"] * x["quantity"], +, transactions)
        push!(df, [token, sum_price])
    end
    return df
end

function accounttablefromcsv_v12()
    trades = loadcsv2df(CSV_TRADES)
    savings = loadcsv2df(CSV_SAVINGS_ACCOUNT)
    spot = loadcsv2df(CSV_SPOT_ACCOUNT)
    staking = loadcsv2df(CSV_STAKING_ACCOUNT)
    return accounttable_v12(trades, savings, spot, staking)
end

function accounttablefromcsv_v3()
    trades = loadcsv2df(CSV_TRADES)
    spot = loadcsv2df(CSV_SPOT_ACCOUNT)
    searn = loadcsv2df(CSV_SIMPLEEARN_ACCOUNT)
    return accounttable(trades, spot, searn)
end

"""
    accounttable_v12(tradesdf, savingsdf, spotdf, stakingdf)

Account calculation for v1 and v2.
"""
function accounttable_v12(tradesdf, savingsdf, spotdf, stakingdf)
    trades = tradesdf
    ib = investmentbalance(trades)
    df = DataFrame(asset=String[], investment=Float64[])
    for coin in IMPORTANT_TOKENS
        invested = filter(row -> row.asset == coin, ib)
        isempty(invested) && continue
        push!(df, [coin, invested[1, :balance]])
    end
    savings = savingsdf
    savings[!, :wallet] .= "Savings"
    spot = spotdf
    spot[!, :wallet] .= "Spot"
    staking = stakingdf
    staking[!, :wallet] .= "Staking"
    alldf = vcat(spot, savings, staking)
    gdf = groupby(alldf, :asset)
    summ = combine(gdf, :quantity => sum)
    rename!(summ, :quantity_sum => :quantity)
    return leftjoin(summ, df, on=:asset)
end

"""
    accounttable(tradesdf, spotdf, searndf)

Account calculation for v3 version, where savings and staking is replaced by simple earn.
"""
function accounttable(tradesdf, spotdf, searndf)
    trades = tradesdf
    ib = investmentbalance(trades)
    df = DataFrame(asset=String[], investment=Float64[])
    for coin in IMPORTANT_TOKENS
        invested = filter(row -> row.asset == coin, ib)
        isempty(invested) && continue
        push!(df, [coin, invested[1, :balance]])
    end
    searn = searndf
    searn[!, :wallet] .= "Simple Earn"
    select!(searn, Not(:simpleearntype))
    spot = spotdf
    spot[!, :wallet] .= "Spot"
    alldf = vcat(spot, searn)
    gdf = groupby(alldf, :asset)
    summ = combine(gdf, :quantity => sum)
    rename!(summ, :quantity_sum => :quantity)
    return leftjoin(summ, df, on=:asset)
end

"""
    balancetable(accountdf, pricedf)

Based on an accountdf and pricedf, calculate
- current value
- pnl (profit and loss)
"""
function balancetable(accountdf, pricedf)
    btdf = leftjoin(accountdf, pricedf, on=:asset)
    btdf[!, :investmentprice] = btdf[!, :investment] ./ btdf[!, :quantity]
    # current value
    btdf[!, :value] = btdf[!, :quantity] .* btdf[!, :price]
    btdf[!, :profit] = btdf[!, :value] - btdf[!, :investment]
    btdf[!, :pnl] = (btdf[!, :value] ./ btdf[!, :investment] .- 1) .* 100
    return select!(btdf, [:asset, :symbol, :quantity, :investment, :investmentprice, :price, :value, :profit, :pnl])
end

"""
    nicetradedf!(tradedf)

Create a nice trade df from a raw trade `DataFrame`.
"""
function nicetradedf!(tradedf)
    # must convert timestamp to julia DateTime
    timestamp2datetime(stamp) = Dates.unix2datetime(stamp / 1000)
    # if already modified, then return it right away
    findfirst(x -> x == "id", names(tradedf)) === nothing && return tradedf
    tradedf[!, :time] = timestamp2datetime.(tradedf[!, :time])
    rename!(tradedf, :symbol => :asset)
    rename!(tradedf, :isBuyer => :action)
    tradedf[!, :action] = (x -> x ? "BUY" : "SELL").(tradedf[!, :action])
    select!(tradedf, [:asset, :action, :time, :price, :qty, :quoteQty])
    return tradedf
end
