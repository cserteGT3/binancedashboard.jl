"""
    BinanceDashboard

Implementing a few Binance API calls with HTTP.jl.
API documentation [link](https://developers.binance.com).
Quite a few functions are copied from [Binance.jl](https://github.com/DennisRutjes/Binance.jl/).
"""
module BinanceDashboard

import HTTP, SHA, JSON3, Dates, Printf
using DataFrames: DataFrame, rename!, select!, innerjoin, groupby, combine, leftjoin, Not
using PrecompileTools
using Logging: @info, @warn
using Base.Threads
import CSV

export  set_TOKENS,
        set_IMPORTANT_TOKENS,
        set_PRICE_SYMBOLS,
        set_SYMBOLS

export  loadapikey,
        alltrades2df,
        spotaccount2df,
        alltrades2csv,
        spotaccount2csv,
        simpleearnaccount2df,
        simpleearnaccount2csv,
        futuresaccount2df,
        futuresaccount2csv,
        loadcsv2df,
        accountbalance,
        investmentbalance,
        accounttable,
        balancetable,
        queryprices!,
        downloadprices,
        prices2csv,
        downloadbinanceaccount2dfs,
        downloadbinanceaccount,
        nicetradedf!

export  TOKENS,
        IMPORTANT_TOKENS,
        SYMBOLS,
        PRICE_SYMBOLS

include("binanceapicalls.jl")
include("apiprocessing.jl")

const APIKEY = Ref(Dict{SubString{String}, SubString{String}}())

## global variables

TOKENS = String[]
IMPORTANT_TOKENS = String[]
PRICE_SYMBOLS = DataFrame(asset = String[], symbol = String[])
SYMBOLS = String[]

const CSV_TRADES = "trades.csv"
const CSV_SPOT_ACCOUNT = "spot_account.csv"
const CSV_FUTURES_ACCOUNT = "futures_account.csv"
const CSV_AUTO_INVEST = "auto_invest.csv"
const CSV_SAVINGS_ACCOUNT = "savings_account.csv"
const CSV_STAKING_ACCOUNT = "staking_account.csv"
const CSV_SIMPLEEARN_ACCOUNT = "simpleearn_account.csv"
const CSV_PRICES = "prices.csv"
# 1 EUR = 1.15 USD ~ "average" in 2021
const EUR2USD = 1.15

## setters

function set_TOKENS(tokens::Vector{String})
    global TOKENS = deepcopy(tokens)
end

function set_IMPORTANT_TOKENS(important_tokens::Vector{String})
    global IMPORTANT_TOKENS = deepcopy(important_tokens)
end

function set_PRICE_SYMBOLS(price_symbols)
    global PRICE_SYMBOLS = deepcopy(price_symbols)
end

function set_SYMBOLS(symbols::Vector{String})
    global SYMBOLS = deepcopy(symbols)
end

"""
    loadapikey(fname="apikey")

Load API key and secret from a file.
Structure:
```
apiKey=...
secretKey=...
```
"""
function loadapikey(fname="apikey")
    if ! isfile(fname)
        throw(ErrorException("""File "$fname" not found."""))
    end
    lines = readlines(fname)
    APIKEY[] = Dict(split.(lines, Ref("=")))
    return nothing
end

try
    loadapikey()
catch
    @warn "Warning: could not load API keys from default file."
end

export templatequery

function templatequery()
    headers = Dict("X-MBX-APIKEY" => String(APIKEY[]["apiKey"]))
    query = string("recvWindow=10000&timestamp=", timestamp())
    r = HTTP.request("GET", string(BINANCE_API_REST, "sapi/v1/lending/union/account?", query, "&signature=", doSign(query)), headers)
    return r2j(r.body)
end

@setup_workload  begin
    # Putting some things in `setup` can reduce the size of the
    # precompile file and potentially make loading faster.
    simpleearn = DataFrame(asset = ["USDT", "SOL"], quantity=[100.0, 3.1415],
        simpleearntype=["flexible", "locked"])
    spot = DataFrame(asset = ["BUSD", "DOGE"], quantity=[10, 2.71])
    trades = DataFrame(symbol=["DOGEBUSD", "SOLUSDT"], price=[0.4, 100], qty=[2.71, 3.1415],
        quoteQty=[1.084, 314.15], comission=[0.0,0.0], commissionAsset=["BNB", "BNB"], time=[1620888934348, 16208836534815],
        isBuyer=[true, true], isMaker=[true, true])
    @compile_workload begin
        # all calls in this block will be precompiled, regardless of whether
        # they belong to your package or not (on Julia 1.8 and higher)
        if Base.isinteractive()
            prices = downloadprices()
        else
            prices = DataFrame(asset=["DOGE", "SOL", "SHIBA"], symbol=["DOGEBUSD", "SOLBUSD", "SHIBABUSD"], price=[0.01, 110.1, 0.01])
        end
        at = accounttable(trades, spot, simpleearn)
        balancetable(at, prices)
        nicetradedf!(trades)
    end
end

end # module
