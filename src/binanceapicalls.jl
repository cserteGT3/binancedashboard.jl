##################### COPY FROM BINANCE.JL #####################
# source: https://github.com/DennisRutjes/Binance.jl/

# base URL of the Binance API
const BINANCE_API_REST = "https://api.binance.com/"
const BINANCE_API_FUTURES = "https://fapi.binance.com/"
const BINANCE_API_TICKER = string(BINANCE_API_REST, "api/v1/ticker/")
const BINANCE_API_DEPTH = string(BINANCE_API_REST, "api/v1/depth")
const BINANCE_API_KLINES = string(BINANCE_API_REST, "api/v1/klines")

export  ping,
        serverTime,
        get24HR,
        getDepth,
        getAllPrices,
        getPrice,
        getAllBookTickers,
        getExchangeInfo,
        getKlines,
        getAccount,
        getAvgPrice,
        getMyTrades,
        getAllOrders,
        getDustLog,
        getSpotBalance,
        getFuturesBalance,
        getFuturesAccountInfo,
        getFlexibleSimpleEarnAccount,
        getLockedSimpleEarnAccount


function dict2Params(dict::Dict)
    params = ""
    for kv in dict
        params = string(params, "&$(kv[1])=$(kv[2])")
    end
    params[2:end]
end

# signing with apiKey and apiSecret
function timestamp()
    Int64(floor(Dates.datetime2unix(Dates.now(Dates.UTC)) * 1000))
end

function doSign(queryString)
    bytes2hex(SHA.hmac_sha256(Vector{UInt8}(APIKEY[]["secretKey"]), Vector{UInt8}(queryString)))
end


# function HTTP response 2 JSON
function r2j(response)
    JSON3.read(String(response))
end

##################### PUBLIC CALL's #####################

# Simple test if binance API is online
function ping()
    r = HTTP.request("GET", string(BINANCE_API_REST, "api/v3/ping"))
    r.status
end

# Binance servertime
function serverTime()
    r = HTTP.request("GET", string(BINANCE_API_REST, "api/v3/time"))
    result = r2j(r.body)

    return (status=r.status, data=Dates.unix2datetime(result.serverTime / 1000))
end

function get24HR()
    r = HTTP.request("GET", string(BINANCE_API_TICKER, "24hr"))
    return (status=r.status, data=r2j(r.body))
end

function getDepth(symbol::String; limit=100) # 500(5), 1000(10)
    r = HTTP.request("GET", string(BINANCE_API_DEPTH, "?symbol=", symbol,"&limit=",limit))
    return (status=r.status, data=r2j(r.body))
end

function get24HR(symbol::String)
    r = HTTP.request("GET", string(BINANCE_API_TICKER, "24hr?symbol=", symbol))
    return (status=r.status, data=r2j(r.body))
end

function getAllPrices()
    r = HTTP.request("GET", string(BINANCE_API_TICKER, "allPrices"))
    return (status=r.status, data=r2j(r.body))
end

function getPrice(symbol::String)
    r = HTTP.request("GET", string(BINANCE_API_TICKER, "price?symbol=", symbol))
    return (status=r.status, data=r2j(r.body))
end


function getAllBookTickers()
    r = HTTP.request("GET", string(BINANCE_API_TICKER, "allBookTickers"))
    return (status=r.status, data=r2j(r.body))
end

function getExchangeInfo()
    r = HTTP.request("GET", string(BINANCE_API_REST, "api/v3/exchangeInfo"))
    return (status=r.status, data=r2j(r.body))
end

function getAvgPrice(symbol::String)
    r = HTTP.request("GET", string(BINANCE_API_REST, "api/v3/avgPrice?symbol=", symbol))
    return (status=r.status, data=r2j(r.body))
end

# binance get candlesticks/klines data
function getKlines(symbol; startDateTime=nothing, endDateTime=nothing, interval="1m")
    query = string("?symbol=", symbol, "&interval=", interval)

    if startDateTime !== nothing && endDateTime !== nothing
        startTime = Printf.@sprintf("%.0d",Dates.datetime2unix(startDateTime) * 1000)
        endTime = Printf.@sprintf("%.0d",Dates.datetime2unix(endDateTime) * 1000)
        query = string(query, "&startTime=", startTime, "&endTime=", endTime)
    end
    r = HTTP.request("GET", string(BINANCE_API_KLINES, query))
    return (status=r.status, data=r2j(r.body))
end

##################### SECURED CALL's NEEDS apiKey / apiSecret #####################

# account call contains balances
function getAccount()
    headers = Dict("X-MBX-APIKEY" => String(APIKEY[]["apiKey"]))

    query = string("recvWindow=20000&timestamp=", timestamp())
    r = HTTP.request("GET", string(BINANCE_API_REST, "api/v3/account?", query, "&signature=", doSign(query)), headers)

    return (status=r.status, data=r2j(r.body))
end

# myTrades call contains all trades with a certain symbol
function getMyTrades(symbol::String)
    headers = Dict("X-MBX-APIKEY" => String(APIKEY[]["apiKey"]))

    query = string("symbol=$symbol&recvWindow=20000&timestamp=", timestamp())
    r = HTTP.request("GET", string(BINANCE_API_REST, "api/v3/myTrades?", query, "&signature=", doSign(query)), headers)

    return (status=r.status, data=r2j(r.body))
end

# allOrders call contains all orders with a certain symbol
function getAllOrders(symbol::String)
    headers = Dict("X-MBX-APIKEY" => String(APIKEY[]["apiKey"]))

    query = string("symbol=$symbol&recvWindow=20000&timestamp=", timestamp())
    r = HTTP.request("GET", string(BINANCE_API_REST, "api/v3/allOrders?", query, "&signature=", doSign(query)), headers)

    return (status=r.status, data=r2j(r.body))
end

# DustLog call contains all dust conversions
function getDustLog()
    headers = Dict("X-MBX-APIKEY" => String(APIKEY[]["apiKey"]))

    query = string("recvWindow=20000&timestamp=", timestamp())
    r = HTTP.request("GET", string(BINANCE_API_REST, "sapi/v1/asset/dribblet?", query, "&signature=", doSign(query)), headers)

    return (status=r.status, data=r2j(r.body))
end

function getSpotBalance()
    headers = Dict("X-MBX-APIKEY" => String(APIKEY[]["apiKey"]))

    query = string("recvWindow=20000&timestamp=", timestamp())
    r = HTTP.request("POST", string(BINANCE_API_REST, "/sapi/v3/asset/getUserAsset?", query, "&signature=", doSign(query)), headers)

    return r2j(r.body)
end

function getFuturesBalance()
    headers = Dict("X-MBX-APIKEY" => String(APIKEY[]["apiKey"]))

    query = string("recvWindow=20000&timestamp=", timestamp())
    r = HTTP.request("GET", string(BINANCE_API_FUTURES, "fapi/v2/balance?", query, "&signature=", doSign(query)), headers)

    return r2j(r.body)
end

function getFuturesAccountInfo()
    headers = Dict("X-MBX-APIKEY" => String(APIKEY[]["apiKey"]))

    query = string("recvWindow=20000&timestamp=", timestamp())
    r = HTTP.request("GET", string(BINANCE_API_FUTURES, "fapi/v2/account?", query, "&signature=", doSign(query)), headers)

    return r2j(r.body)
end

function getFlexibleSimpleEarnAccount()
    headers = Dict("X-MBX-APIKEY" => String(APIKEY[]["apiKey"]))

    query = string("size=20&recvWindow=20000&timestamp=", timestamp())
    r = HTTP.request("GET", string(BINANCE_API_REST, "sapi/v1/simple-earn/flexible/position?", query, "&signature=", doSign(query)), headers)

    return r2j(r.body)
end

function getLockedSimpleEarnAccount()
    headers = Dict("X-MBX-APIKEY" => String(APIKEY[]["apiKey"]))

    query = string("size=20&recvWindow=20000&timestamp=", timestamp())
    r = HTTP.request("GET", string(BINANCE_API_REST, "sapi/v1/simple-earn/locked/position?", query, "&signature=", doSign(query)), headers)

    return r2j(r.body)
end
