# BinanceDashboard.jl

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://csertegt3.gitlab.io/BinanceDashboard.jl/)
[![Build Status](https://gitlab.com/cserteGT3/BinanceDashboard.jl/badges/master/pipeline.svg)](https://gitlab.com/cserteGT3/binancedashboard.jl/pipelines)
[![Coverage](https://img.shields.io/codecov/c/gitlab/cserteGT3/binancedashboard.jl)](https://app.codecov.io/gl/cserteGT3/binancedashboard.jl)
