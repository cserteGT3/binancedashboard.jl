using Documenter, BinanceDashboard

makedocs(
    modules = [BinanceDashboard],
    format = Documenter.HTML(; prettyurls = get(ENV, "CI", nothing) == "true"),
    authors = "Tamás Cserteg",
    sitename = "BinanceDashboard.jl",
    pages = Any["index.md"]
    # strict = true,
    # clean = true,
    # checkdocs = :exports,
)
