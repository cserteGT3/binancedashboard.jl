# Developer notes

## Updates

### 2024.01.27.

Removed API calls:

* `getLendingAccount`
* `getStakingPosition`

Removed functions:

* `stakingaccount2df`
* `stakingaccount2csv`
* `savingsaccount2df`
* `savingsaccount2csv`

## API calls

* spot account can be queried with:
  * Daily Account Snapshot (USER_DATA)
  * `GET /sapi/v1/accountSnapshot`
  * it's not the current, but maybe around midnight
* current spot query (only positive):
  * User Asset (USER_DATA)
  * `POST /sapi/v3/asset/getUserAsset`
* current spot query with all coins listed:
  * Account Information (USER_DATA)
  * `GET /api/v3/account`
* current simple earn flexible:
  * Get Flexible Product Position (USER_DATA)
  * `GET /sapi/v1/simple-earn/flexible/position`
* current simple earn locked:
  * Get Locked Product Position (USER_DATA)
  * `GET /sapi/v1/simple-earn/locked/position`
